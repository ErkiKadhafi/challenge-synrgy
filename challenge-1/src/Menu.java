class Menu {
	private String name;
	private Integer price;

	public Menu(String name, Integer price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return this.name;
	}

	public Integer getPrice() {
		return this.price;
	}
}