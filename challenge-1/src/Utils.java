import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class Utils {
	public static void printHeader(String header) {
		System.out.println("==========================");
		System.out.println(header);
		System.out.println("==========================");
		System.out.println();
	}

	public static void addMenu(ArrayList<Menu> menuList) {
		menuList.add(new Menu("Nasi Goreng", 15000));
		menuList.add(new Menu("Mie Goreng", 13000));
		menuList.add(new Menu("Nasi + Ayam", 18000));
		menuList.add(new Menu("Es Teh Manis", 3000));
		menuList.add(new Menu("Es Jeruk", 5000));
	}

	public static void showMenu(ArrayList<Menu> menuList) {
		printHeader("Selamat datang di BinarFud");
		System.out.println("Silahkan pilih makanan:");

		int index = 1;
		for (Menu menu : menuList) {
			System.out.println(index + ". " + menu.getName() + "\t| " + menu.getPrice());
			index++;
		}

		System.out.println("99. Pesan dan Bayar");
		System.out.println("0. Keluar aplikasi");
		System.out.println();
	}

	public static void confirmMenu(Menu menu) {
		printHeader("Berapa pesanan anda");

		System.out.println(menu.getName() + "\t| " + menu.getPrice());
		System.out.println("(Input 0 untuk kembali)");
		System.out.println();
	}

	public static void confirmPayment(Map<String, Order> orderList) {
		printHeader("Konfirmasi & Pembayaran");

		int totalQuantity = 0, totalPrice = 0;
		for (String foodName : orderList.keySet()) {
			int quantity = orderList.get(foodName).getQuantity();
			int price = orderList.get(foodName).getFood().getPrice();
			int finalPrice = quantity * price;

			totalQuantity += quantity;
			totalPrice += finalPrice;
			System.out.println(foodName + "\t " + quantity + "\t" + finalPrice);
		}

		System.out.println("--------------------------------+");

		System.out.println("Total" + "\t\t" + totalQuantity + "\t" + totalPrice);
		System.out.println();

		System.out.println("1. Konfirmasi dan bayar");
		System.out.println("2. Kembali ke menu utama");
		System.out.println("0. Keluar aplikasi");
		System.out.println();
	}

	public static void showInvoice(Map<String, Order> orderList) {
		printHeader("BinarFud");

		System.out.println("Terimakasih sudah memesan di BinarFud");
		System.out.println();

		System.out.println("Dibawah ini adalah pesanan anda");
		System.out.println();

		int totalQuantity = 0, totalPrice = 0;
		for (String foodName : orderList.keySet()) {
			int quantity = orderList.get(foodName).getQuantity();
			int price = orderList.get(foodName).getFood().getPrice();
			int finalPrice = quantity * price;

			totalQuantity += quantity;
			totalPrice += finalPrice;
			System.out.println(foodName + "\t " + quantity + "\t" + finalPrice);
		}

		System.out.println("--------------------------------+");

		System.out.println("Total" + "\t\t" + totalQuantity + "\t" + totalPrice);
		System.out.println();

		System.out.println("Pembayaran : BinarCash");
		System.out.println();

		Utils.printHeader("Simpan struk ini sebagai\nbukti pembayaran");
	}

	public static void createInvoice(Map<String, Order> orderList) {
		try {
			String fileName = "invoice_" + new SimpleDateFormat("yyyy-MM-dd hh-mm-ss'.txt'").format(new Date());
			FileWriter myWriter = new FileWriter(fileName);

			myWriter.write("==========================\n");
			myWriter.write("BinarFud\n");
			myWriter.write("==========================\n\n");

			myWriter.write("Terimakasih sudah memesan di BinarFud\n\n");

			myWriter.write("Dibawah ini adalah pesanan anda\n\n");

			int totalQuantity = 0, totalPrice = 0;
			for (String foodName : orderList.keySet()) {
				int quantity = orderList.get(foodName).getQuantity();
				int price = orderList.get(foodName).getFood().getPrice();
				int finalPrice = quantity * price;

				totalQuantity += quantity;
				totalPrice += finalPrice;
				myWriter.write(foodName + "\t " + quantity + "\t" + finalPrice + "\n");
			}
			myWriter.write("--------------------------------+\n");

			myWriter.write("Total" + "\t\t" + totalQuantity + "\t" + totalPrice + "\n\n");

			myWriter.write("Pembayaran : BinarCash\n\n");

			myWriter.write("==========================\n");
			myWriter.write("Simpan struk ini sebagai\n");
			myWriter.write("bukti pembayaran\n");
			myWriter.write("==========================");
			myWriter.close();

			System.out.println("Riwayat telah disimpan di " + fileName);
			System.out.println();
		} catch (IOException e) {
			System.out.println("Terjadi kesalahan.");
			e.printStackTrace();
		}
	}

}
